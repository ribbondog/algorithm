package cn.wolfcode.algorithm;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.yaml.snakeyaml.util.ArrayUtils;

import java.util.Arrays;
import java.util.concurrent.CountDownLatch;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AlgorithmApplicationTests {

    @Test
    public void bubbleTest1() {
        /**
         * 冒泡排序,原理比较相邻两个数值,大的放在后面
         */
        int[] arr = new int[]{2, 5, 3, 9, 7, 1};
        System.out.println(Arrays.toString(arr));
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                // 如果前面的数值比较大,那么交换顺序
                if (arr[i] > arr[j]) {
                    swap(arr, i, j);
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }


    @Test
    public void bubbleTest2() {
        /**
         * 冒泡排序,原理比较相邻两个数值,大的放在后面
         */
        int[] arr = new int[]{2, 5, 3, 9, 7, 1};
        System.out.println(Arrays.toString(arr));

        System.out.println(Arrays.toString(arr));
    }


    // 定义方法进行交换
    private void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }


    @Test
    public void threadTest1() {

        CountDownLatch latch = new CountDownLatch(5);
        for (int i = 0; i < 5; i++) {
            Thread t = new Thread() {
                public void run() {
                    try {
                        System.out.println("开启线程啦!");
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        latch.countDown();
                    }
                }
            };
            t.start();
        }
        System.out.println("终于结束啦!");
    }

    @Test
    public void name() {
    }
}
